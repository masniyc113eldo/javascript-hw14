const themeButton = document.querySelector('.change-theme');
const savedTheme = localStorage.getItem('theme');

function applyTheme(theme) {
    document.body.classList.toggle('dark-theme', theme === 'dark');
}

if (savedTheme) {
    applyTheme(savedTheme);
}

themeButton.addEventListener('click', () => {
    const currentTheme = document.body.classList.contains('dark-theme') ? 'blue' : 'dark';
    applyTheme(currentTheme);

    localStorage.setItem('theme', currentTheme);
});
